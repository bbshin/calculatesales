package jp.alhinc.matsunaga_shinya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		//コマンドライン引数の数が1以外の場合エラー文を表示
			if(args.length != 1) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

		//支店定義ファイルのディレクトリパスを変数に格納
		String dir = args[0];

		Map<String, String> branchesMap = new HashMap<>();
		Map<String, Long> salesMap = new HashMap<>();

//1.------------------------------------------------------
		if(!input(dir, "branch.lst", branchesMap, salesMap)) {
			return;
		}
//--------------------------------------------------------

//2.------------------------------------------------------

		BufferedReader br = null;
		try {
			//ディレクトリからrcdかつ数字8桁のファイルを検索、Listで保持
			List<File> rcdIntFileList = new ArrayList<>();
			File allFiles = new File(dir);
			File[] FileLists = allFiles.listFiles();
			for(File fl : FileLists) {
				if(fl.isFile() && fl.getName().matches("[0-9]{8}.rcd")) {
					rcdIntFileList.add(fl);
				}
			}

			//ファイル名が連番であるかチェック
			for(int i=0; i<rcdIntFileList.size(); i++) {
				String file = rcdIntFileList.get(i).getName();
				String int8 = file.substring(0, 8);
				int in = Integer.parseInt(int8);
				if(in - i != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			//合計金額計算
			String line;
			List<String> salesList = new ArrayList<>();
			for(int i=0; i<rcdIntFileList.size(); i++) {
				String file = rcdIntFileList.get(i).getName();

				//rcdファイルの中身を抽出
				FileReader fr = new FileReader(rcdIntFileList.get(i));
				br = new BufferedReader(fr);
				while((line = br.readLine()) != null) {
					salesList.add(line);
				}

				//売上ファイルのフォーマットチェック
				if(salesList.size() != 2) {
					System.out.println(file + "のフォーマットが不正です");
					return;
				}

				//支店コードの有無チェック
				if(!salesMap.containsKey(salesList.get(0))) {
					System.out.println(file + "の支店コードが不正です");
					return;
				}

				//売上金額に数字以外が使われていないかチェック
				String figure = salesList.get(1);
				if(!figure.matches("[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long date1 = Long.parseLong(salesList.get(1));
				long date2 = salesMap.get(salesList.get(0));
				long sum = date1 + date2;


				//合計金額の桁数チェック
				if(sum > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				salesMap.put(salesList.get(0), sum);
				salesList.clear();
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
//--------------------------------------------------------

//3.------------------------------------------------------

		//集計結果出力
		if(!output(dir, "branch.out", branchesMap, salesMap)){
			return;
		}

//--------------------------------------------------------
	}

//メソッド分け--------------------------------------------

	//出力メソッド
	static boolean output(String filePath, String fileName, Map<String, String> branch, Map<String, Long> sales) {
		PrintWriter pw = null;
		try {
			File File = new File(filePath, fileName);
			FileWriter fw = new FileWriter(File);
			BufferedWriter bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);
			for(Map.Entry<String, String> bra : branch.entrySet()) {
				pw.println(bra.getKey() + "," + bra.getValue() + "," + sales.get(bra.getKey()));
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(pw != null) {
				pw.close();
			}
		}
		return true;
	}
	//

	//入力メソッド
	static boolean input(String filePath, String filename, Map<String, String>branch, Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			//支店定義ファイルの読み込み
			File file = new File(filePath, filename);

			//支店定義ファイルの有無をチェック
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			//支店コード：支店名をMapに格納
			String line;
			while((line = br.readLine()) != null){
				String[] str = line.split(",");
				//フォーマットのチェック
				if(str.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				//支店コードに数字以外が使われていないかチェック
				if(!str[0].matches("[0-9]{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				branch.put(str[0], str[1]);
				sales.put(str[0], 0L);
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	//
}
